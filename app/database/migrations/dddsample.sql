CREATE DATABASE dddsample;
GRANT SELECT,INSERT,UPDATE,DELETE,INDEX,ALTER,CREATE,DROP on dddsample.* TO `dddsample`@`127.0.0.1` identified BY 'dddsample';
GRANT SELECT,INSERT,UPDATE,DELETE,INDEX,ALTER,CREATE,DROP on dddsample.* TO `dddsample`@`localhost` identified BY 'dddsample';
FLUSH PRIVILEGES;

CREATE TABLE `members` (
  `member_id` varchar(32) BINARY NOT NULL,
  `member_status` varchar(24) NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`member_id`),
  KEY `member_status` (`member_status`),
  KEY `created_at` (`created_at`),
  KEY `updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `member_registration` (
  `member_id` varchar(32) BINARY NOT NULL,
  `user_agent` varchar(255),
  `remote_address` varchar(255),
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`member_id`),
  KEY `user_agent` (`user_agent`),
  KEY `remote_address` (`remote_address`),
  KEY `created_at` (`created_at`),
  KEY `updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `member_emails` (
  `member_id` varchar(32) BINARY NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`member_id`),
  UNIQUE KEY `email` (`email`),
  KEY `created_at` (`created_at`),
  KEY `updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `member_nicknames` (
  `member_id` varchar(32) BINARY NOT NULL,
  `nickname` varchar(255) BINARY NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`member_id`),
  UNIQUE KEY `nickname` (`nickname`),
  KEY `created_at` (`created_at`),
  KEY `updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `member_passwords` (
  `member_id` varchar(32) BINARY NOT NULL,
  `password_hash` varchar(60) BINARY NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`member_id`),
  KEY `password_hash` (`password_hash`),
  KEY `created_at` (`created_at`),
  KEY `updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `login_sessions` (
  `login_session_id` varchar(64) BINARY NOT NULL,
  `login_device` varchar(24) BINARY NOT NULL,
  `member_id` varchar(32) BINARY NOT NULL,
  `user_agent` varchar(255),
  `expires` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`login_session_id`),
  KEY `login_device` (`login_device`),
  KEY `member_id` (`member_id`),
  KEY `expires` (`expires`),
  KEY `created_at` (`created_at`),
  KEY `updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
