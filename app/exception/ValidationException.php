<?php
namespace DddSample\App\Exception;

/**
 * 検証例外クラス
 *
 * @category dddsample
 * @package  DddSample\App\Exception
 * @author   keita-nishimoto
 * @since    2015-02-03
 */
class ValidationException extends \DddSample\App\Exception\DddSampleException
{
    /**
     * バリデータークラス
     *
     * @var \Illuminate\Validation\Validator
     */
    private $validator;

    /**
     * エラーメッセージ一覧
     *
     * @var array
     */
    protected $errorMessages = [
        '11000' => 'パラメータが正しく設定されていません。',
    ];

    /**
     * コンストラクタ
     *
     * @param string     $errorCode
     * @param \Exception $previous
     */
    public function __construct($errorCode = '11000', $previous = null)
    {
        parent::__construct(
            $errorCode,
            $previous
        );
    }

    /**
     * バリデータークラス設定メソッド
     *
     * @param \Illuminate\Validation\Validator $validator
     */
    public function setValidator($validator)
    {
        $this->validator = $validator;
    }

    /**
     * バリデータークラス取得メソッド
     *
     * @return \Illuminate\Validation\Validator
     */
    public function getValidator()
    {
        return $this->validator;
    }
}