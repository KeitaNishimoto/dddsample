<?php
namespace DddSample\App\Exception;

/**
 * データベース例外クラス
 *
 * @category dddsample
 * @package  DddSample\App\Exception
 * @author   keita-nishimoto
 * @since    2015-02-03
 */
class DbException extends \DddSample\App\Exception\DddSampleException
{

    /**
     * エラーメッセージ一覧
     *
     * @var array
     */
    protected $errorMessages = [
        '20000' => 'データベースに異常が発生しました。',
    ];
}
