<?php
namespace DddSample\App\Exception;

/**
 * サービス例外クラス
 *
 * @category dddsample
 * @package  DddSample\App\Exception
 * @author   keita-nishimoto
 * @since    2015-02-03
 */
class ServiceException extends \DddSample\App\Exception\DddSampleException
{

    /**
     * エラーメッセージ一覧
     *
     * @var array
     */
    protected $errorMessages = [
        '30000' => '対象のサービスクラスが存在しません。',
        '30001' => '対象のメソッドが存在しません。',
    ];
}