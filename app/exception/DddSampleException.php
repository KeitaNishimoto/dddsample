<?php
namespace DddSample\App\Exception;

/**
 * 例外クラス
 *
 * @category dddsample
 * @package  DddSample\App\Exception
 * @author   keita-nishimoto
 * @since    2015-02-03
 */
class DddSampleException extends \Exception
{

    /**
     * エラーメッセージ一覧
     *
     * @var array
     */
    protected $errorMessages = [
        '10000' => 'システムエラーが発生しました。',
        '40000' => 'メールアドレスは登録済です。',
        '40001' => 'ニックネームは登録済です。',
    ];

    /**
     * エラーコード
     *
     * @var string
     */
    protected $errorCode;

    /**
     * コンストラクタ
     *
     * @param string     $errorCode
     * @param \Exception $previous
     */
    public function __construct($errorCode, $previous = null)
    {
        $this->errorCode = $errorCode;

        parent::__construct(
            $this->errorMessages[$this->errorCode],
            $this->errorCode,
            $previous
        );
    }

    /**
     * エラーコード取得メソッド
     *
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * エラーメッセージ取得メソッド
     *
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessages[$this->errorCode];
    }
}
