<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'BlogController@index');
Route::get('blog/index', 'BlogController@index');
Route::get('blog/members', 'BlogController@members');
Route::get('blog/detail', 'BlogController@detail');
Route::get('blog/posts', 'BlogController@posts');
Route::get('member/entry', 'MemberController@entry');
Route::post('member/entry', 'MemberController@entry');
Route::post('member/entry/confirm', 'MemberController@entryConfirm');
Route::post('member/entry/completion', 'MemberController@entryCompletion');
Route::get('login', 'LoginController@index');
Route::post('login', 'LoginController@index');