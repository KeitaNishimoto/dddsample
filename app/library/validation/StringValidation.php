<?php
namespace DddSample\App\Library\Validation;

/**
 * 文字列検証クラス
 *
 * @category   dddsample
 * @package    DddSample\App\Library\Validation
 * @author     keita-nishimoto
 * @since      2015-02-03
 */
class StringValidation
{
    /**
     * ひらがな(UTF-8)正規表現
     *
     * @const string
     */
    const REGEX_UTF8_KANA = '/^(?:\xE3\x81[\x81-\xBF]|\xE3\x82[\x80-\x93])+$/';

    /**
     * 文字列長範囲チェックメソッド
     *
     * @param  string $value
     * @param  int    $minLength
     * @param  int    $maxLength
     * @return boolean
     */
    public static function checkLengthRange($value, $minLength = 1, $maxLength = 1)
    {
        if (is_int($minLength) === false || is_int($maxLength) === false) {
            return false;
        }

        if ($minLength > $maxLength) {
            return false;
        }

        if (is_string($value) === false) {
            return false;
        }

        $valueLength = mb_strlen($value);
        if ($valueLength < $minLength || $valueLength > $maxLength) {
            return false;
        }

        return true;
    }

    /**
     * ひらがな(UTF-8)判定メソッド
     *
     * @param  string $value
     * @return boolean
     */
    public static function isUtf8HiraKana($value)
    {
        $checkString = is_string($value);
        if ($checkString === false) {
            return false;
        }

        $pregResult = preg_match(self::REGEX_UTF8_KANA, $value);
        if ($pregResult === 0 || $pregResult === false) {
            return false;
        }

        return true;
    }
}