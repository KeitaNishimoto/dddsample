<?php
namespace DddSample\App\Library\Validation;

/**
 * バリデーション実行クラス
 *
 * @category   dddsample
 * @package    DddSample\App\Library\Validation
 * @author     keita-nishimoto
 * @since      2015-02-03
 */
class Validator
{

    /**
     * 実行メソッド
     *
     * @param  array(
     *     0 => array(
     *         'key'      => 'キー名',
     *         'callback' => 'コールバック関数。true,falseで結果を返す事',
     *     )
     * )
     * @return array 実行結果
     */
    public static function execute(array $validationInfos)
    {
        $validationErrors = [];
        foreach ($validationInfos as $validationInfo) {

            $result = self::doValidate(
                $validationInfo['callback']
            );

            if ($result === false) {
                array_push($validationErrors, $validationInfo['key']);
            }
        }

        return $validationErrors;
    }

    /**
     * バリデーション実行メソッド
     *
     * @param  callable $callback
     * @return boolean  true OR false
     */
    private static function doValidate(callable $callback)
    {
        return call_user_func($callback);
    }
}