<?php
namespace DddSample\App\Library\Validation;

/**
 * 日付日時検証クラス
 *
 * @category   dddsample
 * @package    DddSample\App\Library\Validation
 * @author     keita-nishimoto
 * @since      2015-02-03
 */
class DateValidation
{
    /**
     * 正規表現 - 日付
     *
     * @var string
     */
    const REGEX_DATE = '/^\d{4}-([0][1-9]|[1][012])-([0][1-9]|[1][0-9]|[2][0-9]|[3][01])$/';

    /**
     * 正規表現 - 日時（日付）
     *
     * @const string
     */
    const REGEX_DATETIME_D = '/^\d{4}-([0][1-9]|[1][012])-([0][1-9]|[1][0-9]|[2][0-9]|[3][01]) ';

    /**
     * 正規表現 - 日時（時刻）
     *
     * @const string
     */
    const REGEX_DATETIME_T = '([0][0-9]|[1][0-9]|[2][0-3]):[0-5][0-9]:[0-5][0-9]$/';

    /**
     * 有効日付チェックメソッド
     *
     * @param  string  $value
     * @return boolean 判定結果
     */
    public static function isEnableDate($value)
    {
        $pattern = self::REGEX_DATE;
        $regex = preg_match($pattern, $value);
        if ($regex === false) {
            return false;
        } else if ($regex === 0) {
            return false;
        }

        $nums = explode('-', $value);

        // 日付の妥当性チェック
        if (checkdate($nums[1], $nums[2], $nums[0]) === false) {
            return false;
        }
        return true;
    }

    /**
     * 有効日時チェックメソッド
     *
     * @param  string  $value
     * @return boolean 判定結果
     */
    public static function isEnableDateTime($value)
    {
        $pattern = self::REGEX_DATETIME_D . self::REGEX_DATETIME_T;
        $regex = preg_match($pattern, $value);
        if ($regex === false) {
            return false;
        } else if ($regex === 0) {
            return false;
        }

        preg_match('/^\d{4}-\d{2}-\d{2}/', $value, $matches);
        $nums = explode('-', $matches[0]);

        // 日付の妥当性チェック
        if (checkdate($nums[1], $nums[2], $nums[0]) === false) {
            return false;
        }
        return true;
    }
}