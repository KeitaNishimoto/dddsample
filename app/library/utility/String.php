<?php
namespace DddSample\App\Library\Utility;

/**
 * 文字列操作クラス
 *
 * @category dddsample
 * @package  DddSample\App\Library\Utility
 * @author   keita-nishimoto
 * @since    2015-02-03
 */
class String
{

    /**
     * ランダム文字列生成メソッド
     *
     * @param  int    桁数
     * @return string ランダム文字列
     */
    public static function createRandString($length)
    {
        // 乱数に使用する文字
        $element  = 'abcdefghijklmnopqrstuvwxyz';
        $element .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $element .= '0123456789';

        // 1文字ずつ配列に格納
        $randArray = str_split($element);

        // 返却文字列
        $result = '';

        // 乱数ジェネレータ初期化
        mt_srand((double)microtime() * 100000);

        for ($i = 0; $i < $length; $i++) {
            // 配列からランダムに一文字取得して返却文字列に結合
            $result .= $randArray[array_rand($randArray, 1)];
        }
        return $result;
    }

    /**
     * ランダムハッシュ生成メソッド
     *
     * @param  string $algorithm ハッシュアルゴリズム
     * @return string ハッシュ値
     */
    public static function createRandomHash($algorithm = 'sha256')
    {
        return hash($algorithm, uniqid(rand(), true));
    }
}