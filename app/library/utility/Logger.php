<?php
namespace DddSample\App\Library\Utility;

/**
 * デバッグログ操作クラス
 *
 * @category dddsample
 * @package  DddSample\App\Library\Utility
 * @author   keita-nishimoto
 * @since    2015-02-03
 */
class Logger
{
    /**
     * デバッグファイル名フォーマット
     *
     * @var string
     */
    private static $_logFileFormat = '/tmp/%s_dddsample.log';

    /**
     * デバッグファイル保存メソッド
     *
     * @param  mixed 変数(どんな型でも可)
     * @return void
     */
    public static function save($var, $fileNameFormat = null)
    {
        // トレースを取得
        $traces = debug_backtrace();
        $result = array();
        foreach ($traces as $trace) {
            if (isset($trace['file']) === true && isset($trace['line']) === true) {
                $result[] = $trace['file'] . ' ' . $trace['line'];
            }
        }
        $debugTrace = var_export($result, true);

        // 現在時刻の取得
        $timeStamp = date('Ymd');

        // ファイル名の作成
        if (is_null($fileNameFormat) === false) {
            $fileName = sprintf(
                $fileNameFormat,
                $timeStamp
            );

        } else {

            $fileName = sprintf(
                self::$_logFileFormat,
                $timeStamp
            );
        }

        $nowDate   = date('Y-m-d-H:i:s') . "\n\n";
        $debugInfo = var_export($var, true) . "\n\n";
        $value     = $nowDate . $debugInfo . $debugTrace;
        error_log($value."\n\n", 3, $fileName);
    }
}