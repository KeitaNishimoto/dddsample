<?php
namespace DddSample\App\Models\Login;

/**
 * ログインセッション - Entityクラス
 *
 * @category DddSample
 * @package  DddSample\App\Models\Login
 * @author   keita-nishimoto
 * @since    2015-01-19
 */
class LoginSessionEntity
{
    /**
     * デフォルト有効期限
     *
     * @var int
     */
    const EXPIRES_IN = 3600;

    /**
     * ログインセッションID
     *
     * @var string
     */
    private $loginSessionId;

    /**
     * 会員ID
     *
     * @var string
     */
    private $memberId;

    /**
     * 有効期限
     *
     * @var int
     */
    private $expires;

    /**
     * ログインデバイス - 値オブジェクト
     *
     * @var \DddSample\App\Models\Login\LoginDeviceValue
     */
    private $loginDeviceValue;

    /**
     * コンストラクタ
     *
     * @param string $loginSessionId
     */
    public function __construct($loginSessionId, $memberId)
    {
        $this->setLoginSessionId($loginSessionId);
        $this->setMemberId($memberId);
        $this->createExpires();
    }

    /**
     * ログインセッションID設定メソッド
     *
     * @param string $loginSessionId
     */
    private function setLoginSessionId($loginSessionId)
    {
        $this->loginSessionId = $loginSessionId;
    }

    /**
     * ログインセッションID取得メソッド
     *
     * @return string
     */
    public function getLoginSessionId()
    {
        return $this->loginSessionId;
    }

    /**
     * 会員ID設定メソッド
     *
     * @param string $memberId
     */
    private function setMemberId($memberId)
    {
        $this->memberId = $memberId;
    }

    /**
     * 会員ID取得メソッド
     *
     * @return string
     */
    public function getMemberId()
    {
        return $this->memberId;
    }

    /**
     * 有効期限生成メソッド
     */
    private function createExpires()
    {
        $this->expires = time() + self::EXPIRES_IN;
    }

    /**
     * 有効期限設定メソッド
     *
     * @param int $expires
     */
    public function setExpires($expires)
    {
        $this->expires = $expires;
    }

    /**
     * 有効期限取得メソッド
     *
     * @return number
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * ログインデバイス - 値オブジェクト設定メソッド
     *
     * @param \DddSample\App\Models\Login\LoginDeviceValue $loginDeviceValue
     */
    public function setLoginDeviceValue(\DddSample\App\Models\Login\LoginDeviceValue $loginDeviceValue)
    {
        $this->loginDeviceValue = $loginDeviceValue;
    }

    /**
     * ログインデバイス - 値オブジェクト取得メソッド
     *
     * @return \DddSample\App\Models\Login\LoginDeviceValue
     */
    public function getLoginDeviceValue()
    {
        return $this->loginDeviceValue;
    }
}