<?php
namespace DddSample\App\Models\Login;

/**
 * ログインデバイス - 値オブジェクト
 *
 * @category DddSample
 * @package  DddSample\App\Models\Login
 * @author   keita-nishimoto
 * @since    2015-02-23
 */
class LoginDeviceValue
{
    /**
     * デフォルトログインデバイス
     *
     * @var string
     */
    const DEFAULT_LOGIN_DEVICE = 'other';

    /**
     * ユーザーエージェント
     *
     * @var string
     */
    private $userAgent;

    /**
     * ログインデバイス
     *
     * @var string
     */
    private $loginDevice;

    /**
     * ログインデバイス配列
     *
     * @var array
     */
    private static $loginDeviceArray = [
        'Android' => 'android',
        'iPhone'  => 'iphone',
    ];

    /**
     * コンストラクタ
     *
     * @param string $userAgent
     * @param string $loginDevice
     */
    public function __construct($userAgent, $loginDevice = null)
    {
        $this->userAgent = $userAgent;

        if (is_null($loginDevice) === false) {
            $this->userAgentToLoginDevice();
        } else {
            $this->loginDevice = $loginDevice;
        }
    }

    /**
     * ユーザーエージェント → ログインデバイス変換メソッド
     */
    private function userAgentToLoginDevice()
    {
        foreach (self::$loginDeviceArray as $key => $value) {
            if (strpos($this->userAgent, $key) !== false) {
                $this->loginDevice = $value;
                return;
            }
        }

        $this->loginDevice = self::DEFAULT_LOGIN_DEVICE;
    }

    /**
     * ユーザーエージェント取得メソッド
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * ログインデバイス取得メソッド
     *
     * @return string
     */
    public function getLoginDevice()
    {
        return $this->loginDevice;
    }
}