<?php
namespace DddSample\App\Models\Login;

use Symfony\Component\Finder\Comparator\NumberComparator;

/**
 * ログインリポジトリクラス
 *
 * @category DddSample
 * @package  DddSample\App\Models\Login
 * @author   keita-nishimoto
 * @since    2015-02-23
 */
class LoginRepository
{
    /**
     * ログインセッションID桁数
     *
     * @var int
     */
    const LOGIN_SESSION_ID_LENGTH = 64;

    /**
     * ログインセッション生成メソッド
     *
     * @return \DddSample\App\Models\Login\LoginSessionEntity
     */
    public function createLoginSessionEntity(
        \DddSample\App\Models\Member\MemberEntity $memberEntity,
        $params
    ){
        $sql = "SELECT login_session_id FROM login_sessions WHERE login_session_id = :loginSessionId";

        do {
            $loginSessionId = \DddSample\App\Library\Utility\String::createRandString(
                self::LOGIN_SESSION_ID_LENGTH
            );
            $bindings = [
                ':loginSessionId' => $loginSessionId
            ];

            $resultArray = \DB::select($sql, $bindings);
            if (empty($resultArray) === true) {

                $loginSessionEntity = \DddSample\App\Models\Factory\EntityFactory::createLoginSessionEntity(
                    $loginSessionId,
                    $memberEntity->getMemberId()
                );

                $loginDeviceValue = \DddSample\App\Models\Factory\ValueFactory::createLoginDeviceValue(
                    $params['userAgent']
                );

                $loginSessionEntity->setLoginDeviceValue($loginDeviceValue);

                return $loginSessionEntity;
            }

        } while (true);
    }

    /**
     * ログインセッションID指定 - 検索メソッド
     *
     * @param  string $loginSessionId
     * @return \DddSample\App\Models\Login\LoginSessionEntity|NULL
     */
    public function searchLoginSession($loginSessionId)
    {
        $sql = "SELECT "
             .     "login_session_id, "
             .     "login_device, "
             .     "member_id, "
             .     "user_agent, "
             .     "expires, "
             .     "created_at "
             . "FROM "
             .     "login_sessions "
             . "WHERE "
             .     "login_session_id = :loginSessionId";

        $bindings = [
            ':loginSessionId' => $loginSessionId
        ];

        $resultArray = \DB::select($sql, $bindings);
        if (array_key_exists(0, $resultArray) === false) {
            return null;
        }

        $result = $resultArray[0];
        $loginDeviceValue = \DddSample\App\Models\Factory\ValueFactory::createLoginDeviceValue(
            $result->user_agent,
            $result->login_device
        );

        $loginSessionEntity = \DddSample\App\Models\Factory\EntityFactory::createLoginSessionEntity(
            $result->login_session_id,
            $result->member_id
        );

        $loginSessionEntity->setLoginDeviceValue($loginDeviceValue);
        $loginSessionEntity->setExpires($result->expires);

        return $loginSessionEntity;
    }
}