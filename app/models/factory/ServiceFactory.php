<?php
namespace DddSample\App\Models\Factory;

/**
 * サービス生成クラス
 *
 * @category DddSample
 * @package  DddSample\App\Models\Factory
 * @author   keita-nishimoto
 * @since    2015-02-03
 */
class ServiceFactory
{

    /**
     * サービスインスタンス格納配列
     *
     * @var array
     */
    private static $instancePool;

    /**
     * サービスクラス名フォーマット
     *
     * @var string
     */
    private static $serviceClassFormat = '\\DddSample\\App\\Models\\%s\\%sService';

    /**
     * 実行メソッド名
     *
     * @var string
     */
    private static $executeMethod;

    /**
     * インスタンス生成メソッド
     *
     * @param  string $serviceName   サービス名
     * @param  string $executeMethod 実行メソッド名
     * @throws \DddSample\App\Exception\ServiceException
     * @return \DddSample\App\Models\ServiceAbstract
     */
    public static function create($serviceName, $executeMethod)
    {
        $serviceName = ucfirst($serviceName);

        $className = sprintf(
            self::$serviceClassFormat,
            $serviceName,
            $serviceName
        );

        if (class_exists($className) === false) {
            $exception = new \DddSample\App\Exception\ServiceException('30000');
            throw $exception;
        }

        if (is_null(self::$instancePool[$serviceName]) === false) {
            if (method_exists(self::$instancePool[$serviceName], $executeMethod) === false) {
                $exception = new \DddSample\App\Exception\ServiceException('30001');
                throw $exception;
            }
            return self::$instancePool[$serviceName];
        }

        $responseId = \DddSample\App\Library\Utility\String::createRandomHash();
        $service = new $className($responseId);
        if (method_exists($service, $executeMethod) === false) {
            $exception = new \DddSample\App\Exception\ServiceException('30001');
            throw $exception;
        }

        self::$instancePool[$serviceName] = $service;

        return self::$instancePool[$serviceName];
    }
}