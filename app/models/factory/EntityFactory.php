<?php
namespace DddSample\App\Models\Factory;

/**
 * エンティティ生成クラス
 *
 * @category DddSample
 * @package  DddSample\App\Models\Factory
 * @author   keita-nishimoto
 * @since    2015-01-19
 */
class EntityFactory
{

    /**
     * エンティティオブジェクトの配列
     *
     * @var array
     */
    private static $instancePool = [];

    /**
     * 会員Entityクラス生成メソッド
     *
     * @param  string $memberId
     * @return \DddSample\App\Models\Member\MemberEntity
     */
    public static function createMemberEntity($memberId)
    {
        $instanceKey = 'MemberEntity' . $memberId;
        if (array_key_exists($instanceKey, self::$instancePool) === true) {
            return self::$instancePool[$instanceKey];
        }

        self::$instancePool[$instanceKey] = new \DddSample\App\Models\Member\MemberEntity($memberId);
        return self::$instancePool[$instanceKey];
    }

    /**
     * ログインセッションEntityクラス生成メソッド
     *
     * @param  string $loginSessionId
     * @param  string $memberId
     * @return \DddSample\App\Models\Login\LoginSessionEntity
     */
    public static function createLoginSessionEntity($loginSessionId, $memberId)
    {
        $instanceKey = 'LoginSessionEntity' . $loginSessionId . $memberId;
        if (array_key_exists($instanceKey, self::$instancePool) === true) {
            return self::$instancePool[$instanceKey];
        }

        self::$instancePool[$instanceKey] = new \DddSample\App\Models\Login\LoginSessionEntity(
            $loginSessionId,
            $memberId
        );
        return self::$instancePool[$instanceKey];
    }
}