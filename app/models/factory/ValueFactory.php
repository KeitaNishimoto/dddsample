<?php
namespace DddSample\App\Models\Factory;

/**
 * 値オブジェクト生成クラス
 *
 * @category DddSample
 * @package  DddSample\App\Models\Factory
 * @author   keita-nishimoto
 * @since    2015-01-19
 */
class ValueFactory
{

    /**
     * メールアドレス - 値オブジェクト生成メソッド
     *
     * @param  string $email
     * @return \DddSample\App\Models\Member\EmailValue
     */
    public static function createEmailValue($email = null)
    {
        $emailValue = new \DddSample\App\Models\Member\EmailValue($email);
        return $emailValue;
    }

    /**
     * 名前 - 値オブジェクト生成メソッド
     *
     * @param  string $nickname
     * @return \DddSample\App\Models\Member\NameValue
     */
    public static function createNameValue($nickname)
    {
        $nameValue = new \DddSample\App\Models\Member\NameValue($nickname);
        return $nameValue;
    }

    /**
     * パスワード - 値オブジェクト生成メソッド
     *
     * @param  string $password
     * @param  string $passwordHash
     * @param  string $memberId
     * @return \DddSample\App\Models\Auth\PasswordValue
     */
    public static function createPasswordValue($password = null, $passwordHash = null, $memberId = null)
    {
        $passwordValue = new \DddSample\App\Models\Auth\PasswordValue($password, $passwordHash, $memberId);
        return $passwordValue;
    }

    /**
     * 会員登録 - 値オブジェクト生成メソッド
     *
     * @param  string $userAgent
     * @param  string $remoteAddress
     * @param  string $registrationDate
     * @return \DddSample\App\Models\Member\RegistrationValue
     */
    public static function createRegistrationValue($userAgent, $remoteAddress, $registrationDate)
    {
        $registrationValue = new \DddSample\App\Models\Member\RegistrationValue(
            $userAgent,
            $remoteAddress,
            $registrationDate
        );

        return $registrationValue;
    }

    /**
     * ログインデバイス - 値オブジェクト生成メソッド
     *
     * @param  string $userAgent
     * @param  string $loginDevice
     * @return \DddSample\App\Models\Login\LoginDeviceValue
     */
    public static function createLoginDeviceValue($userAgent, $loginDevice = null)
    {
        $loginDeviceValue = new \DddSample\App\Models\Login\LoginDeviceValue(
            $userAgent,
            $loginDevice
        );

        return $loginDeviceValue;
    }
}