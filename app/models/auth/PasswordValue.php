<?php
namespace DddSample\App\Models\Auth;

/**
 * パスワード - 値オブジェクト
 *
 * @category DddSample
 * @package  DddSample\App\Models\Member
 * @author   keita-nishimoto
 * @since    2015-01-19
 */
class PasswordValue
{

    /**
     * パスワードソルト前置詞
     *
     * @var string
     */
    const PREFIX_PASSWORD_SALT = 'PasswordSaltPrefix';

    /**
     * パスワード
     *
     * @var string
     */
    private $password;

    /**
     * 会員ID
     *
     * @var string
     */
    private $memberId;

    /**
     * パスワードハッシュ
     *
     * @var string
     */
    private $passwordHash;

    /**
     * パスワードソルト
     *
     * @var string
     */
    private $passwordSalt;

    /**
     * コンスタラクタ
     *
     * @param string $password
     * @param string $memberId
     */
    public function __construct($password = null, $passwordHash = null, $memberId = null)
    {
        $this->setPassword($password);
        $this->setPasswordHash($passwordHash);
        $this->setMemberId($memberId);
    }

    /**
     * パスワード設定メソッド
     *
     * @param string $nickname
     */
    private function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * 会員ID設定メソッド
     *
     * @param string $memberId
     */
    private function setMemberId($memberId)
    {
        $this->memberId = $memberId;
    }

    /**
     * パスワードソルト設定メソッド
     */
    private function setPasswordSalt()
    {
        $this->passwordSalt = self::PREFIX_PASSWORD_SALT . $this->memberId;
    }

    /**
     * パスワードハッシュ設定メソッド
     *
     * @param string $passwordHash
     */
    private function setPasswordHash($passwordHash)
    {
        $this->passwordHash = $passwordHash;
    }

    /**
     * パスワードハッシュ生成メソッド
     */
    public function createPasswordHash()
    {
        $options = [
            'salt' => self::PREFIX_PASSWORD_SALT . $this->memberId,
        ];

        $this->passwordHash = password_hash($this->password, PASSWORD_BCRYPT, $options);
    }

    /**
     * パスワードハッシュ取得メソッド
     *
     * @return string
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * パスワード取得メソッド
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}