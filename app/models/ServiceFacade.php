<?php
namespace DddSample\App\Models;

/**
 * サービスファサードクラス
 *
 * @category DddSample
 * @package  DddSample\App\Models
 * @author   keita-nishimoto
 * @since    2015-02-04
 */
class ServiceFacade
{
    /**
     * サービス名
     *
     * @var string
     */
    private $serviceName;

    /**
     * 実行メソッド名
     *
     * @var string
     */
    private $executeMethod;

    /**
     * サービスクラスインスタンス
     *
     * @var \Api\Model\AbstractService
     */
    private $serviceInstance;

    /**
     * 実行パラメーター
     *
     * @var array
     */
    private $params;

    /**
     * コンスタラクタ
     *
     * @return void
     */
    public function __construct($params = array())
    {
        if (empty($params) === true) {
            $params = [];
        }

        $this->setParams($params);
    }

    /**
     * 処理実行メソッド
     *
     * @return array 実行結果
     */
    public function execute()
    {
        $responseBody = [];

        // トランザクション開始
        \DB::beginTransaction();

        try {
            $serviceInstance = \DddSample\App\Models\Factory\ServiceFactory::create(
                $this->getServiceName(),
                $this->getExecuteMethod()
            );

            $this->setServiceInstance($serviceInstance);

            $responseBody = $this->doLogic();

            // レスポンス生成
            $serviceInstance->setResponseCode('0');
            $serviceInstance->setResponseBody($responseBody);
            $response = $serviceInstance->createResponse();

            // コミット
            \DB::commit();

        } catch (\DddSample\App\Exception\ValidationException $e) {
            // ロールバック
            \DB::rollback();

            $serviceInstance->setResponseCode('1');
            $serviceInstance->setErrorCode($e->getCode());
            $serviceInstance->setErrorMessage($e->getMessage());
            $serviceInstance->setValidator($e->getValidator());
            $response = $serviceInstance->createResponse();

        } catch (\Exception $e) {
            // ロールバック
            \DB::rollback();

            // エラーログ出力
            \DddSample\App\Library\Utility\Logger::save(
                $e->getFile() . ' ' . $e->getLine() . ' ' . $e->getCode() . ' ' . $e->getMessage()
            );

            $serviceInstance->setResponseCode('1');
            $serviceInstance->setErrorCode($e->getCode());
            $serviceInstance->setErrorMessage($e->getMessage());
            $response = $serviceInstance->createResponse();
        }

        return $response;
    }

    /**
     * サービス名設定メソッド
     *
     * @param string $serviceName
     */
    public function setServiceName($serviceName)
    {
        $this->serviceName = $serviceName;
    }

    /**
     * サービス名取得メソッド
     *
     * @return string
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }

    /**
     * 実行メソッド名設定メソッド
     *
     * @param string $executeMethod
     */
    public function setExecuteMethod($executeMethod)
    {
        $this->executeMethod = $executeMethod;
    }

    /**
     * 実行メソッド名取得メソッド
     *
     * @return string
     */
    public function getExecuteMethod()
    {
        return $this->executeMethod;
    }

    /**
     * サービスクラスインスタンス設定メソッド
     *
     * @param object $serviceInstance
     */
    private function setServiceInstance($serviceInstance)
    {
        $this->serviceInstance = $serviceInstance;
    }

    /**
     * サービスクラスインスタンス取得メソッド
     *
     * @return object
     */
    private function getServiceInstance()
    {
        return $this->serviceInstance;
    }

    /**
     * 実行パラメーター設定メソッド
     *
     * @param array $params
     */
    private function setParams($params)
    {
        $this->params = $params;
    }

    /**
     * 実行パラメーター取得メソッド
     *
     * @return array
     */
    private function getParams()
    {
        return $this->params;
    }

    /**
     * サービスロジック実行メソッド
     *
     * @return array 実行結果
     */
    private function doLogic()
    {
        $serviceInstance = $this->getServiceInstance();
        $executeMethod   = $this->getExecuteMethod();

        return $serviceInstance->$executeMethod(
            $this->getParams()
        );
    }
}