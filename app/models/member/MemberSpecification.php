<?php
namespace DddSample\App\Models\Member;

/**
 * 会員 - 仕様クラス
 *
 * @category DddSample
 * @package  DddSample\App\Models\Member
 * @author   keita-nishimoto
 * @since    2015-02-11
 */
class MemberSpecification
{
    /**
     * 会員 - Entityクラス
     *
     * @var \DddSample\App\Models\Member\MemberEntity
     */
    private $memberEntity;

    /**
     * バリデーション定義
     *
     * @var array
     */
    private static $validationRules = [
        'email'    => 'required|email',
        'password' => 'required|alpha|between:8,20',
        'nickname' => 'required|between:1,50',
    ];

    /**
     * コンストラクタ
     *
     * @param \DddSample\App\Models\Member\MemberEntity $memberEntity
     */
    public function __construct(\DddSample\App\Models\Member\MemberEntity $memberEntity)
    {
        $this->setMemberEntity($memberEntity);
    }

    /**
     * 会員 - Entityクラス設定メソッド
     *
     * @param \DddSample\App\Models\Member\MemberEntity $memberEntity
     */
    public function setMemberEntity(\DddSample\App\Models\Member\MemberEntity $memberEntity)
    {
        $this->memberEntity = $memberEntity;
    }

    /**
     * 会員 - Entityクラス取得メソッド
     *
     * @return \DddSample\App\Models\Member\MemberEntity
     */
    public function getMemberEntity()
    {
        return $this->memberEntity;
    }

    /**
     * 検証メソッド
     *
     * @throws \DddSample\App\Exception\ValidationException
     */
    public function validation()
    {
        $validator = \Validator::make(
            [
                'email'    => $this->memberEntity->getEmailValue()->getEmail(),
                'password' => $this->memberEntity->getPasswordValue()->getPassword(),
                'nickname' => $this->memberEntity->getNameValue()->getNickname(),
            ],

            self::$validationRules
        );

        if ($validator->passes() === false) {
            $validationException = new \DddSample\App\Exception\ValidationException();
            $validationException->setValidator($validator);
            throw $validationException;
        }
    }

}