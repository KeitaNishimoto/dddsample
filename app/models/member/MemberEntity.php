<?php
namespace DddSample\App\Models\Member;

/**
 * 会員 - Entityクラス
 *
 * @category DddSample
 * @package  DddSample\App\Models\Member
 * @author   keita-nishimoto
 * @since    2015-01-19
 */
class MemberEntity
{
    /**
     * 会員ID
     *
     * @var string
     */
    private $memberId;

    /**
     * 会員ステータス
     *
     * @var string
     */
    private $memberStatus;

    /**
     * メールアドレス - 値オブジェクト
     *
     * @var \DddSample\App\Models\Member\EmailValue
     */
    private $emailValue;

    /**
     * パスワード - 値オブジェクト
     *
     * @var \DddSample\App\Models\Auth\PasswordValue
     */
    private $passwordValue;

    /**
     * 名前 - 値オブジェクト
     *
     * @var \DddSample\App\Models\Member\NameValue
     */
    private $nameValue;

    /**
     * 会員登録 - 値オブジェクト
     *
     * @var \DddSample\App\Models\Member\RegistrationValue
     */
    private $registrationValue;

    /**
     * コンストラクタ
     *
     * @param string $memberId
     */
    public function __construct($memberId)
    {
        $this->setMemberId($memberId);
    }

    /**
     * 会員ID設定メソッド
     *
     * @param string $memberId
     */
    private function setMemberId($memberId)
    {
        $this->memberId = $memberId;
    }

    /**
     * 会員ID取得メソッド
     *
     * @return string
     */
    public function getMemberId()
    {
        return $this->memberId;
    }

    /**
     * 会員ステータス設定メソッド
     *
     * @param string $memberStatus
     */
    public function setMemberStatus($memberStatus)
    {
        $this->memberStatus = $memberStatus;
    }

    /**
     * 会員ステータス取得メソッド
     *
     * @return string
     */
    public function getMemberStatus()
    {
        return $this->memberStatus;
    }

    /**
     * メールアドレス - 値オブジェクト設定メソッド
     *
     * @param \DddSample\App\Models\Member\EmailValue $emailValue
     */
    public function setEmailValue(\DddSample\App\Models\Member\EmailValue $emailValue)
    {
        $this->emailValue = $emailValue;
    }

    /**
     * メールアドレス - 値オブジェクト取得メソッド
     *
     * @return \DddSample\App\Models\Member\EmailValue
     */
    public function getEmailValue()
    {
        return $this->emailValue;
    }

    /**
     * パスワード - 値オブジェクト設定メソッド
     *
     * @param \DddSample\App\Models\Auth\PasswordValue $passwordValue
     */
    public function setPasswordValue(\DddSample\App\Models\Auth\PasswordValue $passwordValue)
    {
        $this->passwordValue = $passwordValue;
    }

    /**
     * パスワード - 値オブジェクト取得メソッド
     *
     * @return \DddSample\App\Models\Auth\PasswordValue
     */
    public function getPasswordValue()
    {
        return $this->passwordValue;
    }

    /**
     * 名前 - 値オブジェクト設定メソッド
     *
     * @param \DddSample\App\Models\Member\NameValue $nameValue
     */
    public function setNameValue(\DddSample\App\Models\Member\NameValue $nameValue)
    {
        $this->nameValue = $nameValue;
    }

    /**
     * 名前 - 値オブジェクト取得メソッド
     *
     * @return \DddSample\App\Models\Member\NameValue
     */
    public function getNameValue()
    {
        return $this->nameValue;
    }

    /**
     * 会員登録 - 値オブジェクト設定メソッド
     *
     * @param \DddSample\App\Models\Member\RegistrationValue $registrationValue
     */
    public function setRegistrationValue($registrationValue)
    {
        $this->registrationValue = $registrationValue;
    }

    /**
     * 会員登録 - 値オブジェクト取得メソッド
     *
     * @return \DddSample\App\Models\Member\RegistrationValue
     */
    public function getRegistrationValue()
    {
        return $this->registrationValue;
    }

}