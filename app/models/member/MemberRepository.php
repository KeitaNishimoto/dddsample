<?php
namespace DddSample\App\Models\Member;

use Symfony\Component\Finder\Comparator\NumberComparator;

/**
 * 会員リポジトリクラス
 *
 * @category DddSample
 * @package  DddSample\App\Models\Member
 * @author   keita-nishimoto
 * @since    2015-02-11
 */
class MemberRepository
{
    /**
     * 会員ID桁数
     *
     * @var int
     */
    const MEMBER_ID_LENGTH = 32;

    /**
     * デフォルト会員ステータス
     *
     * @var string
     */
    const DEFAULT_MEMBER_STATUS = 'active';

    /**
     * 会員生成メソッド
     *
     * @return \DddSample\App\Models\Member\MemberEntity
     */
    public function createMember($params)
    {
        $sql = "SELECT member_id FROM members WHERE member_id = :memberId";

        do {
            $memberId = \DddSample\App\Library\Utility\String::createRandString(self::MEMBER_ID_LENGTH);
            $bindings = [
                ':memberId' => $memberId
            ];

            $resultArray = \DB::select($sql, $bindings);
            if (empty($resultArray) === true) {
                $memberEntity = \DddSample\App\Models\Factory\EntityFactory::createMemberEntity(
                    $memberId
                );

                $memberEntity->setMemberStatus(self::DEFAULT_MEMBER_STATUS);

                $emailValue = \DddSample\App\Models\Factory\ValueFactory::createEmailValue(
                    $params['email']
                );

                $passwordValue = \DddSample\App\Models\Factory\ValueFactory::createPasswordValue(
                    $params['password'],
                    null,
                    $memberId
                );
                $passwordValue->createPasswordHash();

                $nameValue = \DddSample\App\Models\Factory\ValueFactory::createNameValue(
                    $params['nickname']
                );

                $registrationValue = \DddSample\App\Models\Factory\ValueFactory::createRegistrationValue(
                    $params['userAgent'],
                    $params['remoteAddress'],
                    date('Y-m-d H:i:s')
                );

                $memberEntity->setEmailValue($emailValue);
                $memberEntity->setPasswordValue($passwordValue);
                $memberEntity->setNameValue($nameValue);
                $memberEntity->setRegistrationValue($registrationValue);

                return $memberEntity;
            }

        } while (true);
    }

    /**
     * メールアドレス指定 - 検索メソッド
     *
     * @param  string $email
     * @return \DddSample\App\Models\Member\MemberEntity|NULL
     */
    public function searchMemberByEmail($email, $password = null)
    {
        $sql = "SELECT "
             .     "m.member_id, "
             .     "m.member_status, "
             .     "me.email, "
             .     "mn.nickname, "
             .     "mp.password_hash, "
             .     "mr.user_agent, "
             .     "mr.remote_address, "
             .     "mr.created_at "
             . "FROM "
             .     "members AS m "
             . "INNER JOIN "
             .     "member_emails AS me "
             . "USING(member_id) "
             . "INNER JOIN "
             .     "member_nicknames AS mn "
             . "USING(member_id) "
             . "INNER JOIN "
             .     "member_passwords AS mp "
             . "USING(member_id) "
             . "INNER JOIN "
             .     "member_registration AS mr "
             . "USING(member_id) "
             . "WHERE "
             .     "me.email = :email";

        $bindings = [
            ':email' => $email
        ];

        $resultArray = \DB::select($sql, $bindings);
        if (array_key_exists(0, $resultArray) === false) {
            return null;
        }

        $result = $resultArray[0];
        $emailValue = \DddSample\App\Models\Factory\ValueFactory::createEmailValue(
            $email
        );

        $passwordValue = \DddSample\App\Models\Factory\ValueFactory::createPasswordValue(
            $password,
            $result->password_hash,
            $result->member_id
        );

        $nameValue = \DddSample\App\Models\Factory\ValueFactory::createNameValue(
            $result->nickname
        );

        $registrationValue = \DddSample\App\Models\Factory\ValueFactory::createRegistrationValue(
            $result->user_agent,
            $result->remote_address,
            $result->created_at
        );

        $memberEntity = \DddSample\App\Models\Factory\EntityFactory::createMemberEntity(
            $result->member_id
        );

        $memberEntity->setMemberStatus($result->member_status);
        $memberEntity->setEmailValue($emailValue);
        $memberEntity->setPasswordValue($passwordValue);
        $memberEntity->setNameValue($nameValue);
        $memberEntity->setRegistrationValue($registrationValue);

        return $memberEntity;
    }

    /**
     * ニックネーム指定 - 検索メソッド
     *
     * @param  string $nickname
     * @return \DddSample\App\Models\Member\MemberEntity|NULL
     */
    public function searchMemberByNickname($nickname, $password = null)
    {
        $sql = "SELECT "
             .     "m.member_id, "
             .     "m.member_status, "
             .     "me.email, "
             .     "mn.nickname, "
             .     "mp.password_hash, "
             .     "mr.user_agent, "
             .     "mr.remote_address, "
             .     "mr.created_at "
             . "FROM "
             .     "members AS m "
             . "INNER JOIN "
             .     "member_emails AS me "
             . "USING(member_id) "
             . "INNER JOIN "
             .     "member_nicknames AS mn "
             . "USING(member_id) "
             . "INNER JOIN "
             .     "member_passwords AS mp "
             . "USING(member_id) "
             . "INNER JOIN "
             .     "member_registration AS mr "
             . "USING(member_id) "
             . "WHERE "
             .     "mn.nickname = :nickname";

        $bindings = [
            ':nickname' => $nickname
        ];

        $resultArray = \DB::select($sql, $bindings);
        if (array_key_exists(0, $resultArray) === false) {
            return null;
        }

        $result = $resultArray[0];
        $emailValue = \DddSample\App\Models\Factory\ValueFactory::createEmailValue(
            $result->email
        );

        $passwordValue = \DddSample\App\Models\Factory\ValueFactory::createPasswordValue(
            $password,
            $result->password_hash,
            $result->member_id
        );

        $nameValue = \DddSample\App\Models\Factory\ValueFactory::createNameValue(
            $result->nickname
        );

        $registrationValue = \DddSample\App\Models\Factory\ValueFactory::createRegistrationValue(
            $result->user_agent,
            $result->remote_address,
            $result->created_at
        );

        $memberEntity = \DddSample\App\Models\Factory\EntityFactory::createMemberEntity(
            $result->member_id
        );

        $memberEntity->setMemberStatus($result->member_status);
        $memberEntity->setEmailValue($emailValue);
        $memberEntity->setPasswordValue($passwordValue);
        $memberEntity->setNameValue($nameValue);
        $memberEntity->setRegistrationValue($registrationValue);

        return $memberEntity;
    }

    /**
     * 登録メソッド
     *
     * @param \DddSample\App\Models\Member\MemberEntity $memberEntity
     */
    public function entry(\DddSample\App\Models\Member\MemberEntity $memberEntity)
    {
        $this->entryMembers($memberEntity);
        $this->entryMemberEmails($memberEntity);
        $this->entryMemberNicknames($memberEntity);
        $this->entryMemberPasswords($memberEntity);
        $this->entryMemberRegistration($memberEntity);
    }

    /**
     * 登録メソッド
     *
     * members
     *
     * @param \DddSample\App\Models\Member\MemberEntity $memberEntity
     */
    private function entryMembers(\DddSample\App\Models\Member\MemberEntity $memberEntity)
    {
        $values = [
            'member_id'     => $memberEntity->getMemberId(),
            'member_status' => $memberEntity->getMemberStatus(),
            'created_at'    => $memberEntity->getRegistrationValue()->getRegistrationDate(),
        ];

        \DB::table('members')->insert(
            $values
        );
    }

    /**
     * 登録メソッド
     *
     * member_emails
     *
     * @param \DddSample\App\Models\Member\MemberEntity $memberEntity
     */
    private function entryMemberEmails(\DddSample\App\Models\Member\MemberEntity $memberEntity)
    {
        $values = [
            'member_id'  => $memberEntity->getMemberId(),
            'email'      => $memberEntity->getEmailValue()->getEmail(),
            'created_at' => $memberEntity->getRegistrationValue()->getRegistrationDate(),
        ];

        \DB::table('member_emails')->insert(
            $values
        );
    }

    /**
     * 登録メソッド
     *
     * member_nicknames
     *
     * @param \DddSample\App\Models\Member\MemberEntity $memberEntity
     */
    private function entryMemberNicknames(\DddSample\App\Models\Member\MemberEntity $memberEntity)
    {
        $values = [
            'member_id'  => $memberEntity->getMemberId(),
            'nickname'  => $memberEntity->getNameValue()->getNickname(),
            'created_at' => $memberEntity->getRegistrationValue()->getRegistrationDate(),
        ];

        \DB::table('member_nicknames')->insert(
            $values
        );
    }

    /**
     * 登録メソッド
     *
     * member_passwords
     *
     * @param \DddSample\App\Models\Member\MemberEntity $memberEntity
     */
    private function entryMemberPasswords(\DddSample\App\Models\Member\MemberEntity $memberEntity)
    {
        $values = [
            'member_id'     => $memberEntity->getMemberId(),
            'password_hash' => $memberEntity->getPasswordValue()->getPasswordHash(),
            'created_at'    => $memberEntity->getRegistrationValue()->getRegistrationDate(),
        ];

        \DB::table('member_passwords')->insert(
            $values
        );
    }

    /**
     * 登録メソッド
     *
     * member_registration
     *
     * @param \DddSample\App\Models\Member\MemberEntity $memberEntity
     */
    private function entryMemberRegistration(\DddSample\App\Models\Member\MemberEntity $memberEntity)
    {
        $values = [
            'member_id'      => $memberEntity->getMemberId(),
            'user_agent'     => $memberEntity->getRegistrationValue()->getUserAgent(),
            'remote_address' => $memberEntity->getRegistrationValue()->getRemoteAddress(),
            'created_at'     => $memberEntity->getRegistrationValue()->getRegistrationDate(),
        ];

        \DB::table('member_registration')->insert(
            $values
        );
    }

}