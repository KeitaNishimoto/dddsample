<?php
namespace DddSample\App\Models\Member;

/**
 * メールアドレス - 値オブジェクト
 *
 * @category DddSample
 * @package  DddSample\App\Models\Member
 * @author   keita-nishimoto
 * @since    2015-01-19
 */
class EmailValue
{
    /**
     * メールアドレス
     *
     * @var string
     */
    private $email;

    /**
     * コンスタラクタ
     *
     * @param string $email
     */
    public function __construct($email = null)
    {
        $this->setEmail($email);
    }

    /**
     * メールアドレス設定メソッド
     *
     * @param string $email
     */
    private function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * メールアドレス取得メソッド
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}