<?php
namespace DddSample\App\Models\Member;

/**
 * 会員登録 - 値オブジェクト
 *
 * @category DddSample
 * @package  DddSample\App\Models\Member
 * @author   keita-nishimoto
 * @since    2015-02-19
 */
class RegistrationValue
{
    /**
     * ユーザーエージェント
     *
     * @var string
     */
    private $userAgent;

    /**
     * リモートアドレス
     *
     * @var string
     */
    private $remoteAddress;

    /**
     * 会員登録日時
     *
     * @var string
     */
    private $registrationDate;

    /**
     * コンストラクタ
     *
     * @param string $userAgent
     * @param string $remoteAddress
     * @param string $memberEntryDate
     */
    public function __construct($userAgent, $remoteAddress, $registrationDate)
    {
        $this->userAgent        = $userAgent;
        $this->remoteAddress    = $remoteAddress;
        $this->registrationDate = $registrationDate;
    }

    /**
     * ユーザーエージェント取得メソッド
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * リモートアドレス取得メソッド
     *
     * @return string
     */
    public function getRemoteAddress()
    {
        return $this->remoteAddress;
    }

    /**
     * 会員登録日時取得メソッド
     *
     * @return string
     */
    public function getRegistrationDate()
    {
        return $this->registrationDate;
    }
}