<?php
namespace DddSample\App\Models\Member;

/**
 * 会員サービスクラス
 *
 * @category DddSample
 * @package  DddSample\App\Models\Member
 * @author   keita-nishimoto
 * @since    2015-02-04
 */
class MemberService extends \DddSample\App\Models\ServiceAbstract
{
    /**
     * 会員リポジトリクラス
     *
     * @var \DddSample\App\Models\Member\MemberRepository
     */
    private $memberRepository;

    /**
     * 登録チェックメソッド
     *
     * @param  array $params
     * @throws \DddSample\App\Exception\DddSampleException
     * @return boolean
     */
    public function entryCheck($params)
    {
        $this->_entryCheck($params);
        return true;
    }

    /**
     * 登録チェックメソッド
     *
     * @param  array $params
     * @throws \DddSample\App\Exception\DddSampleException
     * @return \DddSample\App\Models\Member\MemberEntity
     */
    private function _entryCheck($params)
    {
        $this->memberRepository = new \DddSample\App\Models\Member\MemberRepository();
        $memberEntity = $this->memberRepository->searchMemberByEmail($params['email']);
        if ($memberEntity instanceof \DddSample\App\Models\Member\MemberEntity) {
            throw new \DddSample\App\Exception\DddSampleException(
                '40000'
            );
        }

        $memberEntity = $this->memberRepository->searchMemberByNickname(
            $params['nickname']
        );

        if ($memberEntity instanceof \DddSample\App\Models\Member\MemberEntity) {
            throw new \DddSample\App\Exception\DddSampleException(
                '40001'
            );
        }

        $memberEntity = $this->memberRepository->createMember($params);
        $memberSpecification = new \DddSample\App\Models\Member\MemberSpecification(
            $memberEntity
        );
        $memberSpecification->validation();

        return $memberEntity;
    }

    /**
     * 登録メソッド
     *
     * @param  array $params
     * @throws \DddSample\App\Exception\DddSampleException
     * @return \DddSample\App\Models\Member\MemberEntity
     */
    public function entryCompletion($params)
    {
        $memberEntity = $this->_entryCheck($params);
        $this->memberRepository->entry($memberEntity);

        return $memberEntity;
    }
}