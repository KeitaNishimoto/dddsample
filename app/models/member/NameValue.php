<?php
namespace DddSample\App\Models\Member;

/**
 * 名前 - 値オブジェクト
 *
 * @category DddSample
 * @package  DddSample\App\Models\Member
 * @author   keita-nishimoto
 * @since    2015-01-19
 */
class NameValue
{
    /**
     * ニックネーム
     *
     * @var string
     */
    private $nickname;

    /**
     * コンスタラクタ
     *
     * @param string $nickname
     */
    public function __construct($nickname = null)
    {
        $this->setNickname($nickname);
    }

    /**
     * ニックネーム設定メソッド
     *
     * @param string $nickname
     */
    private function setNickname($nickname)
    {
        $this->nickname = $nickname;
    }

    /**
     * ニックネーム取得メソッド
     *
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }
}