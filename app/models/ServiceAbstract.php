<?php
namespace DddSample\App\Models;

/**
 * サービス抽象化クラス
 *
 * @category DddSample
 * @package  DddSample\App\Models
 * @author   keita-nishimoto
 * @since    2015-02-04
 */
abstract class ServiceAbstract
{
    /**
     * レスポンスID
     *
     * @var string
     */
    private $responseId;

    /**
     * レスポンスコード
     * 正常終了時には0、異常終了時に1
     *
     * @var string
     */
    private $responseCode;

    /**
     * バリデータークラス
     *
     * @var \Illuminate\Validation\Validator
     */
    private $validator;

    /**
     * レスポンスボディ
     *
     * @var array
     */
    private $responseBody;

    /**
     * エラーコード
     *
     * @var string
     */
    private $errorCode;

    /**
     * エラーメッセージ
     *
     * @var string
     */
    private $errorMessage;

    /**
     * コンストラクタ
     *
     * @return void
     */
    final public function __construct($responseId)
    {
        $this->setResponseId($responseId);
        $this->setResponseCode('1');
    }

    /**
     * レスポンス生成メソッド
     *
     * @return array
     */
    final public function createResponse()
    {
        $response = [
            'header' => [
                'responseId'   => $this->getResponseId(),
                'responseCode' => $this->getResponseCode(),
            ],
            'body' => [],
        ];

        if ($this->getResponseCode() === '1') {
            $response['body']['errorCode']    = $this->getErrorCode();
            $response['body']['errorMessage'] = $this->getErrorMessage();

            $validator = $this->getValidator();
            if (is_null($validator) === false) {
                $response['body']['validator'] = $validator;
            }

        } else {
            $response['body'] = $this->getResponseBody();
        }

        return $response;
    }

    /**
     * レスポンスID設定メソッド
     *
     * @param string $responseId
     */
    private function setResponseId($responseId)
    {
        $this->responseId = $responseId;
    }

    /**
     * レスポンスID取得メソッド
     *
     * @return string
     */
    final public function getResponseId()
    {
        return $this->responseId;
    }

    /**
     * レスポンスコード設定メソッド
     *
     * @param string $responseCode
     */
    final public function setResponseCode($responseCode)
    {
        $this->responseCode = $responseCode;
    }

    /**
     * レスポンスコード取得メソッド
     *
     * @return string
     */
    final public function getResponseCode()
    {
        return $this->responseCode;
    }

    /**
     * バリデーションエラー情報設定メソッド
     *
     * @param \Illuminate\Validation\Validator $validator
     */
    final public function setValidator($validator)
    {
        $this->validator = $validator;
    }

    /**
     * バリデーションエラー情報取得メソッド
     *
     * @return \Illuminate\Validation\Validator
     */
    final public function getValidator()
    {
        return $this->validator;
    }

    /**
     * レスポンスボディ設定メソッド
     *
     * @param array $responseBody
     */
    final public function setResponseBody($responseBody)
    {
        $this->responseBody = $responseBody;
    }

    /**
     * レスポンスボディ取得メソッド
     *
     * @return array
     */
    final public function getResponseBody()
    {
        return $this->responseBody;
    }

    /**
     * エラーコード設定メソッド
     *
     * @param string $errorCode
     */
    final public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * エラーコード取得メソッド
     *
     * @return string
     */
    final public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * エラーメッセージ設定メソッド
     *
     * @param string $errorMessage
     */
    final public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * エラーメッセージ取得メソッド
     *
     * @return string
     */
    final public function getErrorMessage()
    {
        return $this->errorMessage;
    }
}