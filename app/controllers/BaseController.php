<?php
/**
 * コントローラー基底クラス
 *
 * @category DddSample
 * @package  DddSample
 * @author   keita-nishimoto
 * @since    2015-02-23
 */
class BaseController extends Controller {

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if ( ! is_null($this->layout))
        {
            $this->layout = View::make($this->layout);
        }
    }
}
