<?php
/**
 * 会員 - コントローラークラス
 *
 * @category DddSample
 * @package  DddSample
 * @author   keita-nishimoto
 * @since    2015-02-23
 */
class MemberController extends BaseController
{

    /**
     * 登録メソッド
     *
     * @return \Illuminate\View\View
     */
    public function entry()
    {
        $csrfToken = \DddSample\App\Library\Utility\String::createRandomHash();
        Cookie::queue(
            'csrfToken',
            $csrfToken,
            time() + 3600
        );

        $entryParams = [
            'title'     => '会員登録',
            'email'     => Input::get('email'),
            'password'  => Input::get('password'),
            'nickname'  => Input::get('nickname'),
            'csrfToken' => $csrfToken,
        ];

        return View::make('member/entry')->with($entryParams);
    }

    /**
     * 登録確認メソッド
     *
     * @return \Illuminate\View\View
     */
    public function entryConfirm()
    {
        $entryParams = [
            'title'         => '会員登録確認',
            'email'         => Input::get('email'),
            'password'      => Input::get('password'),
            'nickname'      => Input::get('nickname'),
            'csrfToken'     => Input::get('csrfToken'),
            'userAgent'     => Request::server('HTTP_USER_AGENT'),
            'remoteAddress' => Request::server('REMOTE_ADDR'),
        ];

        // CSRF対策
        if ($entryParams['csrfToken'] !== Cookie::get('csrfToken')) {
            return View::make('error/csrf')->with($entryParams);
        }

        if (Input::getMethod() === 'POST') {
            $serviceFacade = new \DddSample\App\Models\ServiceFacade($entryParams);
            $serviceFacade->setServiceName('member');
            $serviceFacade->setExecuteMethod('entryCheck');
            $entryCheckResult = $serviceFacade->execute();

            if ($entryCheckResult['header']['responseCode'] === '1') {
                switch ($entryCheckResult['body']['errorCode']) {
                    case '11000':
                        $validator = $entryCheckResult['body']['validator'];
                        return View::make('member/entry')->with($entryParams)->withErrors($validator);
                        break;
                    case '40000':
                        $entryParams['emailError'] = '';
                        return View::make('member/entry')->with($entryParams);
                        break;
                    case '40001':
                        $entryParams['nicknameError'] = '';
                        return View::make('member/entry')->with($entryParams);
                        break;
                    default:
                        $entryParams['errorCode']    = $entryCheckResult['body']['errorCode'];
                        $entryParams['errorMessage'] = $entryCheckResult['body']['errorMessage'];
                        return View::make('error/system')->with($entryParams);
                        break;
                }
            }
        }

        return View::make('member/entryConfirm')->with($entryParams);
    }

    /**
     * 登録完了メソッド
     *
     * @return \Illuminate\View\View
     */
    public function entryCompletion()
    {
        $entryParams = [
            'title'         => '会員登録完了',
            'email'         => Input::get('email'),
            'password'      => Input::get('password'),
            'nickname'      => Input::get('nickname'),
            'csrfToken'     => Input::get('csrfToken'),
            'userAgent'     => Request::server('HTTP_USER_AGENT'),
            'remoteAddress' => Request::server('REMOTE_ADDR'),
        ];

        // CSRF対策
        if ($entryParams['csrfToken'] !== Cookie::get('csrfToken')) {
            return View::make('error/csrf')->with($entryParams);
        }

        $serviceFacade = new \DddSample\App\Models\ServiceFacade($entryParams);
        $serviceFacade->setServiceName('member');
        $serviceFacade->setExecuteMethod('entryCompletion');
        $entryResult = $serviceFacade->execute();

        if ($entryResult['header']['responseCode'] === '1') {
            $entryParams['errorCode']    = $entryResult['body']['errorCode'];
            $entryParams['errorMessage'] = $entryResult['body']['errorMessage'];
            return View::make('error/system')->with($entryParams);
        }

        return View::make('member/entryCompletion')->with($entryParams);
    }
}