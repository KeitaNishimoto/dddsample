<?php
/**
 * ログイン - コントローラークラス
 *
 * @category DddSample
 * @package  DddSample
 * @author   keita-nishimoto
 * @since    2015-02-23
 */
class LoginController extends BaseController
{

    /**
     * ログインメソッド
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $csrfToken = \DddSample\App\Library\Utility\String::createRandomHash();
            Cookie::queue(
            'csrfToken',
            $csrfToken,
            time() + 3600
        );

        $loginParams = [
            'title'     => 'ログイン',
            'email'     => Input::get('email'),
            'password'  => Input::get('password'),
            'csrfToken' => $csrfToken,
        ];

        return View::make('login/index')->with($loginParams);
    }

}