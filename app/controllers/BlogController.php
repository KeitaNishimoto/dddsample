<?php
/**
 * ブログ - コントローラークラス
 *
 * @category DddSample
 * @package  DddSample
 * @author   keita-nishimoto
 * @since    2015-02-23
 */
class BlogController extends BaseController
{

    /**
     * ブログ一覧メソッド
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $params = [
            'title' => 'ブログ一覧',
        ];

        return View::make('blog/index')->with($params);
    }

    /**
     * 会員ブログ一覧メソッド
     *
     * @return \Illuminate\View\View
     */
    public function members()
    {
        $params = [
            'title' => '会員ブログ一覧',
        ];

        return View::make('blog/members')->with($params);
    }

    /**
     * ブログ詳細メソッド
     *
     * @return \Illuminate\View\View
     */
    public function detail()
    {
        $params = [
            'title' => 'ブログ詳細',
        ];

        return View::make('blog/detail')->with($params);
    }

    /**
     * ブログ投稿メソッド
     *
     * @return \Illuminate\View\View
     */
    public function posts()
    {
        $params = [
            'title' => 'ブログ投稿',
        ];

        return View::make('blog/posts')->with($params);
    }

}