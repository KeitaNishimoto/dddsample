@extends('layout')

@section('content')

<h1>システムエラーが発生しました。</h1>

<h2>{{{ 'エラーコード' . ' ' . $errorCode }}}</h2>

<h2>{{{ 'エラーメッセージ' . ' ' . $errorMessage }}}</h2>

@stop