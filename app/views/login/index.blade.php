@extends('layout')

@section('content')
<form action="/login" method="post" class="form-signin" role="form">
    <div class="form-group {{{ isset($errorMessages) ? 'has-error' : '' }}}">
        <label for="email" class="control-label">
            メールアドレス　{{{ isset($errorMessages) ? 'メールアドレスまたはパスワードが違います。' : '' }}}
        </label>
        <input
            type="email"
            name="email"
            id="email"
            class="form-control"
            value="{{{ $email }}}"
            placeholder="Email address"
        >
    </div>

    <div class="form-group {{{ isset($errorMessages) ? 'has-error' : '' }}}">
        <label for="password" class="control-label">
            パスワード　{{{ isset($errorMessages) ? 'メールアドレスまたはパスワードが違います。' : '' }}}
        </label>
        <input
            type="password"
            id="password"
            name="password"
            class="form-control"
            value="{{{ $password }}}"
            placeholder="Password"
            autocomplete="off"
        >
    </div>

    <div class="form-group" class="control-label">
        <input
            type="hidden"
            id="csrfToken"
            name="csrfToken"
            class="form-control"
            value="{{{ $csrfToken }}}"
        >
    </div>

    <button class="btn btn-lg btn-primary btn-block" type="submit">ログイン</button>
</form>
@stop