<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $title; ?></title>
        {{ HTML::style('tbs/css/bootstrap.min.css') }}
        {{ HTML::style('tbs/css/bootstrap-theme.min.css') }}
    </head>

    <body>
        <!-- header -->
        <div class="navbar navbar-default navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="https://ces.dmm.com/"><i class="glyphicon glyphicon-home"></i></a>
            </div>

            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown ">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown">ドメイン駆動設計サンプル</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-header">ユースケース一覧</li>
                            <li class="divider"></li>
                            <li><a href="/member/entry">会員登録</a></li>
                            <li><a href="/login">ログイン</a></li>
                            <li><a href="/">ブログ一覧</a></li>
                            <li><a href="/blog/posts">ブログ投稿</a></li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="">○○さん</a></li>
                    <li><a href="">ログアウト</a></li>
                </ul>
            </div>
        </div>
        <!-- /header -->
        @yield('content')
        {{ Html::script('http://code.jquery.com/jquery-2.1.3.min.js') }}
        {{ Html::script('tbs/js/bootstrap.min.js') }}
    </body>
</html>