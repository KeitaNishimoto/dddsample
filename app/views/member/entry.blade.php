@extends('layout')

@section('content')
<?php
    $errorMessages = $errors->getMessages();

    if (isset($emailError)) {
        $errorMessages = ['email' => ''];
    }

    if (isset($nicknameError)) {
        $errorMessages = ['nickname' => $nicknameError];
    }
?>
<form action="/member/entry/confirm" method="post" class="form-signin" role="form">
    <div class="form-group {{{ isset($errorMessages['email']) ? 'has-error' : '' }}}">
        <label for="email" class="control-label">
            メールアドレス　{{{ isset($errorMessages['email']) ? 'このメールアドレスは使用出来ません。' : '' }}}
        </label>
        <input
            type="email"
            name="email"
            id="email"
            class="form-control"
            value="{{{ $email }}}"
            placeholder="Email address"
        >
    </div>

    <div class="form-group {{{ isset($errorMessages['password']) ? 'has-error' : '' }}}">
        <label for="password" class="control-label">
            パスワード　{{{ isset($errorMessages['password']) ? 'このパスワードは使用出来ません。' : '' }}}
        </label>
        <input
            type="password"
            id="password"
            name="password"
            class="form-control"
            value="{{{ $password }}}"
            placeholder="Password"
            autocomplete="off"
        >
    </div>

    <div class="form-group {{{ isset($errorMessages['nickname']) ? 'has-error' : '' }}}">
        <label for="nickname" class="control-label">
            ニックネーム　{{{ isset($errorMessages['nickname']) ? 'このニックネームは使用出来ません。' : '' }}}
        </label>
        <input
            type="text"
            id="nickname"
            name="nickname"
            class="form-control"
            value="{{{ $nickname }}}"
            placeholder="nickname"
        >
    </div>

    <div class="form-group" class="control-label">
        <input
            type="hidden"
            id="csrfToken"
            name="csrfToken"
            class="form-control"
            value="{{{ $csrfToken }}}"
        >
    </div>

    <button class="btn btn-lg btn-primary btn-block" type="submit">登録内容を確認</button>
</form>
@stop