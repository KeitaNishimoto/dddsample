@extends('layout')

@section('content')
<p>以下の内容で登録を行います。よろしいですか？</p>

<form action="/member/entry/completion" method="post" class="form-signin" role="form">
    <div class="form-group">
        <label for="view_email">メールアドレス</label>
        <input
            type="email"
            name="view_email"
            id="view_email"
            class="form-control"
            value="<?php echo $email; ?>"
            placeholder="Email address"
            disabled="disabled"
        >
    </div>

    <div class="form-group">
        <label for="view_password">パスワード</label>
        <input
            type="view_password"
            id="view_password"
            name="view_password"
            class="form-control"
            value="<?php echo $password; ?>"
            placeholder="Password"
            autocomplete="off"
            disabled="disabled"
        >
    </div>

    <div class="form-group">
        <label for="view_nickname">ニックネーム</label>
        <input
            type="text"
            id="view_nickname"
            name="view_nickname"
            class="form-control"
            value="<?php echo $nickname; ?>"
            placeholder="nickname"
            disabled="disabled"
        >
    </div>

    <div class="form-group">
        <input
            type="hidden"
            name="email"
            id="email"
            class="form-control"
            value="<?php echo $email; ?>"
        >
    </div>

    <div class="form-group">
        <input
            type="hidden"
            id="password"
            name="password"
            class="form-control"
            value="<?php echo $password; ?>"
        >
    </div>

    <div class="form-group">
        <input
            type="hidden"
            id="nickname"
            name="nickname"
            class="form-control"
            value="<?php echo $nickname; ?>"
        >
    </div>

    <div class="form-group">
        <input
            type="hidden"
            id="csrfToken"
            name="csrfToken"
            class="form-control"
            value="<?php echo $csrfToken; ?>"
        >
    </div>

    <button class="btn btn-lg btn-primary btn-block" type="submit">登録を行う</button>
</form>

@stop